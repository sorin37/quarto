#include <iostream>
#include "Piece.h"
#include "Board.h"
#include "UnusedPieces.h"
#include "../Logging/Logger.h"
#include "Player.h"
int main()
{
	Piece piece(Piece::Body::FULL, Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::SQUARE);
	std::cout << "Full, Dark, Short, Square piece: " << piece << std::endl;

	Board board;
	std::cout << "Empty board:\n" << board << std::endl;

	board[{0, 0}] = std::move(piece);
	std::cout << "Moved piece to board:\n" << board << std::endl;

	UnusedPieces unusedPieces;
	std::cout << "All available pieces:\n" << unusedPieces << std::endl;
	board[{1, 1}] = unusedPieces.PickPiece("0001");
	std::cout << "Extracted \"0001\" remaining pieces after extracted:\n" << unusedPieces << std::endl;
	std::cout << board << std::endl;

	Player player("Sorin");
	player.PlacePiece(std::cin, unusedPieces.PickPiece("1111"), board);

	//std::cout << player.PickPiece(std::cin, unusedPieces) << std::endl << unusedPieces << std::endl;

	
}