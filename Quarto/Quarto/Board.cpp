#include "Board.h"

std::optional<Piece>& Board::operator[](const Position& position)
{
	const auto& [line, column] = position;
	return m_board[m_width * line + column];
}

const std::optional<Piece>& Board::operator[](const Position& position) const
{
	const auto& [line, column] = position;
	return m_board[m_width * line + column];
}
const char emptyBoardCell[] = "____";
std::ostream& operator<<(std::ostream& os, const Board& board)
{
	Board::Position position;
	auto& [line, column] = position;
	for (line = 0; line < Board::m_height; line++) {
		for (column = 0; column < Board::m_width; column++) {
			if (board[position]) {

				os << *board[position];
			}
			else {
				os << emptyBoardCell;
			}
			os << ' ';
		}
		os << std::endl;
	}
	return os;
}
