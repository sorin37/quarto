#pragma once
#include <cstdint>
#include <iostream>
class Piece
{
public:
	enum class Color :uint8_t {
		LIGHT,
		DARK
	};
	enum class Height :uint8_t {
		SHORT,
		TALL
	};
	enum class Shape :uint8_t {
		ROUND,
		SQUARE
	};
	enum class Body :uint8_t {
		HOLLOW,
		FULL
	};
	Piece(Body body, Color color, Height height, Shape shape);
	friend std::ostream& operator<<(std::ostream& f, const Piece& piece);
	Body getBody() const;
	Color getColor() const;
	Height getHeight() const;
	Shape getShape() const;
private:
	char : 4;//biti de padding
	Color m_color : 1;
	Height m_height : 1;
	Shape m_shape : 1;
	Body m_body : 1;
};