#include "QuartoGame.h"
void QuartoGame::Run()
{
	//Step 1: Data initialisation

	UnusedPieces unusedPieces;
	Board board;
	std::string player1Name, player2Name;
	std::cout << "Name of the first player: ";
	std::cin >> player1Name;
	std::cout << "Name of the second player: ";
	std::cin >> player2Name;

	Player player1{ player1Name };
	Player player2{ player2Name };

	std::reference_wrapper<Player> pickingPlayer = player1;
	auto placingPlayer = std::ref(player2);

	//Step 2: Game Loop
	while (true) {
		system("cls");
		std::cout << "Board:\n" << board << "\n";
		std::cout << "UnusedPieces:\n" << unusedPieces;

		std::cout << pickingPlayer << "pick a piece for" << placingPlayer << "\n";

	}
}
