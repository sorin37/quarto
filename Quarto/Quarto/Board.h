#pragma once
#include "Piece.h"
#include <array>
#include <optional>
#include <iostream>
#include <utility>

class Board {
public:
	using Position = std::pair<uint8_t, uint8_t>;
private:
	static const size_t m_width = 4;
	static const size_t m_height = 4;
	static const size_t m_boardSize = m_width * m_height;
	std::array<std::optional<Piece>, m_boardSize> m_board;
public:
	Board() = default;
	std::optional<Piece>& operator[](const Position& position);//write
	const std::optional<Piece>& operator[](const Position& position) const;//read
	friend std::ostream& operator<<(std::ostream& os, const Board &board);

};