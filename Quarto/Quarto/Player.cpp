#include "Player.h"

std::istream& operator>>(std::istream& in, Player& player)
{
	std::cout << "Player name: ";
	in >> player.m_name;

	return in;
}

std::ostream& operator<<(std::ostream& out, const Player& player)
{
	out << player.m_name;

	return out;
}

Player::Player(const std::string& name)
{
	m_name = name;
}

Piece Player::PickPiece(std::istream& in, UnusedPieces& unusedPieces) const
{
	std::string input_text;
	in >> input_text;
	Piece piece = unusedPieces.PickPiece(input_text);
	return piece;
}

Board::Position Player::PlacePiece(std::istream& in, Piece&& piece, Board& board) const
{
	int row, col;
	if (in >> row) {
		if (in >> col) {
			Board::Position position{ static_cast<uint8_t>(row), static_cast<uint8_t>(col) };
			auto& optionalPiece = board[position];
			if (optionalPiece) {
				throw std::exception{ "This position is occupied." };
			}
			optionalPiece = std::move(piece);
			return position;
		}
	}
	in.clear();
	in.seekg(std::ios::end);
	throw  std::exception{ "Please write numbers between 0 and 3" };
}
