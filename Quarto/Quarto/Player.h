#pragma once
#include <string>
#include <iostream>
#include "Piece.h"
#include "UnusedPieces.h"	
#include "Board.h"
class Player
{
private:
	std::string m_name;

public:
	Player() {};
	Player(const std::string& name);

	friend std::istream& operator>>(std::istream& in, Player& player);
	friend std::ostream& operator<<(std::ostream& out, const Player& player);

	Piece PickPiece(std::istream& in, UnusedPieces& unusedPieces) const;
	Board::Position PlacePiece(std::istream &in, Piece&& piece, Board& board) const;
};

