#pragma once
#include "Piece.h"
#include <unordered_map>
#include <string>
#include <sstream>

class UnusedPieces
{
private:
	std::unordered_map<std::string, Piece> m_unusedPieces;
	void GeneratePieces();
	void EmplacePiece(Piece&& piece);

public:
	UnusedPieces();

	friend std::ostream& operator<<(std::ostream& out, const UnusedPieces& unusedPieces);
	Piece PickPiece(const std::string& pieceName);
};

