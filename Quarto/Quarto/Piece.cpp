#include "Piece.h"
Piece::Piece(Body body, Color color, Height height, Shape shape) :m_color{ color }, m_height{ height }, m_shape{ shape }, m_body{ body }
{
	static_assert(sizeof* this <= 1, "This class should be 1 byte in size!");
}

Piece::Body Piece::getBody() const
{
	return m_body;
}

Piece::Color Piece::getColor() const
{
	return m_color;
}

Piece::Height Piece::getHeight() const
{
	return m_height;
}

Piece::Shape Piece::getShape() const
{
	return m_shape;
}

std::ostream& operator<<(std::ostream& f, const Piece& piece)
{
	f << static_cast<int>(piece.m_body)
		<< static_cast<int>(piece.m_color)
		<< static_cast<int>(piece.m_height)
		<< static_cast<int>(piece.m_shape);
	return f;
}
