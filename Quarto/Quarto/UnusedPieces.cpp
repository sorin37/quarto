#include "UnusedPieces.h"

void UnusedPieces::GeneratePieces()
{
	for (int index1 = 0; index1 < 2; index1++)
	{
		for (int index2 = 0; index2 < 2; index2++)
		{
			for (int index3 = 0; index3 < 2; index3++)
			{
				for (int index4 = 0; index4 < 2; index4++)
				{
					EmplacePiece(
						Piece(static_cast<Piece::Piece::Body>(index1),
							static_cast<Piece::Piece::Color>(index2),
							static_cast<Piece::Piece::Height>(index3),
							static_cast<Piece::Piece::Shape>(index4)
							));
				}
			}
		}
	}
}

void UnusedPieces::EmplacePiece(Piece&& piece)
{
	std::stringstream ss;
	ss << piece;

	m_unusedPieces.insert(std::make_pair(ss.str(), piece));
}

UnusedPieces::UnusedPieces()
{
	GeneratePieces();
}

Piece UnusedPieces::PickPiece(const std::string& pieceName)
{
	/*Varianta 1*/
	/*auto iterator = m_unusedPieces.find(pieceName);

	if (iterator != m_unusedPieces.end())
	{
		Piece piece = std::move(iterator->second);
		m_unusedPieces.erase(iterator);
		return piece;
	}
	else
	{
		throw std::exception("Piece not found!");
	}*/

	/*Varianta 2*/
	auto node = m_unusedPieces.extract(pieceName);//iteratorul doar se plimba prin mapa, iar node este un element efectiv din mapa (ciorchine, strugure, etc..)
	if (node)
	{
		return std::move(node.mapped());
	}
	else
	{
		throw std::exception("Piece not found!");
	}
}

std::ostream& operator<<(std::ostream& out, const UnusedPieces& unusedPieces)
{
	/*Varianta 1*/
	/*for (const auto& iterator : unusedPieces.m_unusedPieces)
	{
		out << iterator.first << " ";
	}*/

	/*Varianta 2*/
	for (const auto& [pieceName, pieceObject] : unusedPieces.m_unusedPieces)
	{
		out << pieceName << " ";
	}

	return out;
}
